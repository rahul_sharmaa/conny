
import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { Grid } from 'react-bootstrap';

let Footer = React.createClass({

    render() {
        return (
            <footer className="footer">
                <Grid>
                    <nav className="pull-left">
                        <ul>
                            <li>
                                <a href="#pablo">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="#pablo">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#pablo">
                                    Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#pablo">
                                   Blog
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p className="copyright pull-right">
                        &copy; {(new Date()).getFullYear()} <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                    </p>
                </Grid>
            </footer>
        );
    }
});

let mapStateToProps = (state) => {
   
    return { 

    };
};
let mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch
});
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Footer));
