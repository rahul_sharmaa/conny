
import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import * as screenConfig from './../config/screenConfig';
import { bindActionCreators } from 'redux';
import { Navbar } from 'react-bootstrap';
import HeaderLinks from './headerlinks.jsx';

var logo = require('./../images/logo.png');
let Header = React.createClass({

    render() {
        return (
            <Navbar fluid>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#pablo">{this.getBrand()}</a>
                    </Navbar.Brand>
                    <Navbar.Toggle onClick={this.mobileSidebarToggle}/>
                </Navbar.Header>
                <Navbar.Collapse>
                    <HeaderLinks />
                </Navbar.Collapse>
            </Navbar>
        );
    }
});

let mapStateToProps = (state) => {
   
    return { 

    };
};
let mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch
});
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));
