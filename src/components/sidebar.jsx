import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { bindActionCreators } from 'redux';
import './../sass/employee.scss';
import Header from './header.jsx';
let Sidebar = React.createClass({

    render() {
        return (
            <div className="sidebar-wrapper">
                <Header />
                <ul>
                    <li className="current"><a href="#"><span className="icon-sidebar icon-user"></span><span className="sidebar-list">USERS</span></a></li>
                    <li><a href="#"><span className="icon-sidebar icon-employee"></span><span className="sidebar-list">EMPLOYEES</span></a></li>
                    <li><a href="#"><span className="icon-sidebar icon-attendance"></span><span className="sidebar-list">ATTENDANCE</span></a></li>
                    <li><a href="#"><span className="icon-sidebar icon-sites"></span><span className="sidebar-list">SITES</span></a></li>
                    <li><a href="#"><span className="icon-sidebar icon-payroll"></span><span className="sidebar-list">PAYROLL</span></a></li>
                    <li><a href="#"><span className="icon-sidebar icon-billing"></span><span className="sidebar-list">BILLING</span></a></li>
                    <li><a href="#"><span className="icon-sidebar icon-masters"></span><span className="sidebar-list">MASTERS</span></a></li>
                </ul>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
   
    return {

    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Sidebar));
