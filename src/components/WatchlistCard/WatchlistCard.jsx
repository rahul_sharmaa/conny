import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import Button from '../../elements/CustomButton/CustomButton';

export class WatchlistCard extends Component{
    render(){
        return (
            <div className="card card-stats">
                <div className="content">
                    <Row>
                        <Col xs={12}>
                            <div className="icon-big text-center icon-warning">
                              <img src={this.props.bigIcon} width="100%" />
                            </div>
                            <div className="text">
                                <h3>{this.props.titleText}</h3>
                                <p>{this.props.companyText}</p>
                                <p>{this.props.buildingText}</p>
                            </div>
                        </Col>
                        <Col xs={12}>
                            <div className="numbers">
                                <p>{this.props.statsText}</p>
                                {this.props.statsValue}
                            </div>
                        </Col>
                    </Row>
                    <div className="footer">
                        <hr />
                        <div className="stats">
                            <Button bsStyle="danger">Remove from WatchList</Button>
                            {
                            //{this.props.statsIcon}{" "}{this.props.statsIconText}
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default WatchlistCard;
