import * as EmployeeActions from './../actions/employee_actions';
import * as UserActions from './../actions/user_actions';
import extend from 'lodash/extend';

export var Employees = (state = {
    listEmployees: [],
    currentStep: 1,
    currEmployee: { employee_name: '' },
    empMasterData: {},
    empPersonalData: {},
    empBankData: {},
    empWorkData: {},
    empDocumentData: {},
    empWageData: {},
    fields: [],
    states: [
        { id: 1, name: 'Telangana' },
        { id: 2, name: 'Maharashtra' },
        { id: 3, name: 'Punjab' },
        { id: 4, name: 'Karnataka' },
    ],
    designations: [
        { id: 1, name: 'Accountant' },
        { id: 2, name: 'Security' },
    ],
    cities: [
        { id: 1, state_id: 1, name: 'Hyderabad' },
        { id: 2, state_id: 2, name: 'Pune' },
        { id: 3, state_id: 2, name: 'Mumbai' },
        { id: 4, state_id: 2, name: 'Nasik' },
        { id: 5, state_id: 3, name: 'Amritsar' },
        { id: 6, state_id: 3, name: 'Ludhiana' },
        { id: 7, state_id: 3, name: 'Mohali' },
        { id: 8, state_id: 4, name: 'Bangalore' },
    ],
    maritalStatuses: [
        { id: 0, name: 'Unmarried' },
        { id: 1, name: 'Married' },
    ],
    bloodGroups: [
        { id: 0, name: 'O-' },
        { id: 1, name: 'O+' },
        { id: 2, name: 'A-' },
        { id: 3, name: 'A+' },
        { id: 4, name: 'B-' },
        { id: 5, name: 'B+' },
        { id: 6, name: 'AB-' },
        { id: 7, name: 'AB+' },
    ],
    taxonomies: [
        { id: 1, name: 'Allowance' },
        { id: 2, name: 'Deduction' },
        { id: 3, name: 'Company Contribution' },
    ],
    fields: [
        { id: 1, taxonomy_id: 1, name: "HRA", type: 0, value: 0 },
        { id: 2, taxonomy_id: 1, name: "HDA", type: 0, value: 0 },
        { id: 3, taxonomy_id: 1, name: "Washing", type: 1, value: 0 },
        { id: 4, taxonomy_id: 1, name: "Conveyance", type: 0, value: 0 },
        { id: 5, taxonomy_id: 1, name: "CCA", type: 0, value: 0 },
        { id: 6, taxonomy_id: 1, name: "Additional Salary", type: 0, value: 0 },
        { id: 7, taxonomy_id: 2, name: "PF", type: 0, value: 0 },
        { id: 8, taxonomy_id: 2, name: "ESIC", type: 0, value: 0 },
        { id: 9, taxonomy_id: 2, name: "MLWF", type: 0, value: 0 },
        { id: 10, taxonomy_id: 2, name: "Professional Tax", type: 1, value: 0 },
        { id: 11, taxonomy_id: 3, name: "PF", type: 0, value: 0 },
        { id: 12, taxonomy_id: 3, name: "ESIC", type: 0, value: 0 },
        { id: 13, taxonomy_id: 3, name: "Ex-Gratia", type: 0, value: 0 },
        { id: 14, taxonomy_id: 3, name: "Leave Salary", type: 1, value: 0 },
        { id: 15, taxonomy_id: 3, name: "MLWF", type: 0, value: 0 },
        { id: 16, taxonomy_id: 3, name: "Gratuity", type: 1, value: 0 },
        { id: 17, taxonomy_id: 3, name: "Documentation Charges", type: 1, value: 0 },
        { id: 18, taxonomy_id: 3, name: "Training Charges", type: 1, value: 0 },
        { id: 19, taxonomy_id: 3, name: "Uniforms", type: 1, value: 0 },
    ],
    values: [
        { term_id: 1, value: 0 },
        { term_id: 2, value: 0 },
        { term_id: 3, value: 0 },
        { term_id: 4, value: 0 },
        { term_id: 5, value: 0 },
        { term_id: 6, value: 0 },
        { term_id: 7, value: 0 },
        { term_id: 8, value: 0 },
        { term_id: 9, value: 0 },
        { term_id: 10, value: 0 },
        { term_id: 11, value: 0 },
        { term_id: 12, value: 0 },
        { term_id: 13, value: 0 },
        { term_id: 14, value: 0 },
        { term_id: 15, value: 0 },
        { term_id: 16, value: 0 },
        { term_id: 17, value: 0 },
        { term_id: 18, value: 0 },
        { term_id: 19, value: 0 },
    ],
    currentDocument: 0,
    documents: [],
}, action) => {
    switch (action.type) {
        case 'persist/REHYDRATE': {
            if (action.payload.Employees) {
                return action.payload.Employees;
            } else {
                return state;
            }
        } break;
        case EmployeeActions.LIST_ALL: {
            return extend({}, state, { listEmployees: [] });
        } break;
        case EmployeeActions.LIST_ALL_SUCCESS: {
            return extend({}, state, {
                listEmployees: action.data,
                dataReceivedAt: new Date().getTime()
            });
        } break;
        case EmployeeActions.GET_EMP: {
            return extend({}, state, { currentStep: 1, currEmployee: {} });
        } break;
        case EmployeeActions.GET_EMP_SUCCESS: {
            return extend({}, state, {
                currentStep: 1,
                currEmployee: action.data.data
            });
        } break;
        case EmployeeActions.NEW_EMPLOYEE: {
            return extend({}, state, { currEmployee: {}, currentStep: 1 });
        } break;
        case EmployeeActions.ADD_EMP_STEP: {
            return extend({}, state, { currentStep: action.data });
        } break;
        case EmployeeActions.CRUD_EMP_MASTER: {
            return extend({}, state, {
                empMasterData: {}, currEmployee: {
                    id: state.currEmployee.id,
                    master: action.data,
                    personal: state.currEmployee.personal,
                    bank: state.currEmployee.bank,
                    work: state.currEmployee.work,
                    document: state.currEmployee.document,
                    wage: state.currEmployee.wage,
                }
            });
        } break;
        case EmployeeActions.CRUD_EMP_MASTER_SUCCESS: {
            return extend({}, state, {
                empMasterData: action.data, currEmployee: {
                    id: action.data.data.employee_id,
                    master: state.currEmployee.master,
                    personal: state.currEmployee.personal,
                    bank: state.currEmployee.bank,
                    work: state.currEmployee.work,
                    document: state.currEmployee.document,
                    wage: state.currEmployee.wage,
                }
            });
        } break;
        case EmployeeActions.CRUD_EMP_MASTER_FAILED: {
            return extend({}, state, {});
        } break;
        case EmployeeActions.CRUD_EMP_PERSONAL: {
            return extend({}, state, {
                empPersonalData: {}, currEmployee: {
                    id: state.currEmployee.id,
                    master: state.currEmployee.master,
                    personal: action.data,
                    bank: state.currEmployee.bank,
                    work: state.currEmployee.work,
                    document: state.currEmployee.document,
                    wage: state.currEmployee.wage,
                }
            });
        } break;
        case EmployeeActions.CRUD_EMP_PERSONAL_SUCCESS: {
            return extend({}, state, {
                empPersonalData: { data: action.data }, currEmployee: {
                    id: state.currEmployee.id,
                    master: state.currEmployee.master,
                    personal: state.currEmployee.personal,
                    bank: state.currEmployee.bank,
                    work: state.currEmployee.work,
                    document: state.currEmployee.document,
                    wage: state.currEmployee.wage,
                }
            });
        } break;
        case EmployeeActions.CRUD_EMP_PERSONAL_FAILED: {
            return extend({}, state, {});
        } break;
        case EmployeeActions.CRUD_EMP_BANK: {
            return extend({}, state, {
                empBankData: {}, currEmployee: {
                    id: state.currEmployee.id,
                    master: state.currEmployee.master,
                    personal: state.currEmployee.personal,
                    bank: action.data,
                    work: state.currEmployee.work,
                    document: state.currEmployee.document,
                    wage: state.currEmployee.wage,
                }
            });
        } break;
        case EmployeeActions.CRUD_EMP_BANK_SUCCESS: {
            return extend({}, state, {
                empBankData: { data: action.data }, currEmployee: {
                    id: state.currEmployee.id,
                    master: state.currEmployee.master,
                    personal: state.currEmployee.personal,
                    bank: state.currEmployee.bank,
                    work: state.currEmployee.work,
                    document: state.currEmployee.document,
                    wage: state.currEmployee.wage,
                }
            });
        } break;
        case EmployeeActions.CRUD_EMP_BANK_FAILED: {
            return extend({}, state, {});
        } break;
        case EmployeeActions.CRUD_EMP_WORK: {
            return extend({}, state, {
                empWorkData: {}, currEmployee: {
                    id: state.currEmployee.id,
                    master: state.currEmployee.master,
                    personal: state.currEmployee.personal,
                    bank: state.currEmployee.bank,
                    work: action.data,
                    document: state.currEmployee.document,
                    wage: state.currEmployee.wage,
                }
            });
        } break;
        case EmployeeActions.CRUD_EMP_WORK_SUCCESS: {
            return extend({}, state, {
                empWorkData: { data: action.data }, currEmployee: {
                    id: state.currEmployee.id,
                    master: state.currEmployee.master,
                    personal: state.currEmployee.personal,
                    bank: state.currEmployee.bank,
                    work: state.currEmployee.work,
                    document: state.currEmployee.document,
                    wage: state.currEmployee.wage,
                }
            });
        } break;
        case EmployeeActions.CRUD_EMP_WORK_FAILED: {
            return extend({}, state, {});
        } break;
        case EmployeeActions.CRUD_EMP_DOCUMENT: {
            return extend({}, state, {
                empDocumentData: {}, currEmployee: {
                    id: state.currEmployee.id,
                    master: state.currEmployee.master,
                    personal: state.currEmployee.personal,
                    bank: state.currEmployee.bank,
                    work: state.currEmployee.work,
                    wage: state.currEmployee.wage,
                    document: action.data
                }
            });
        } break;
        case EmployeeActions.CRUD_EMP_DOCUMENT_SUCCESS: {
            return extend({}, state, {
                empDocumentData: { data: action.data }, currEmployee: {
                    id: state.currEmployee.id,
                    master: state.currEmployee.master,
                    personal: state.currEmployee.personal,
                    bank: state.currEmployee.bank,
                    work: state.currEmployee.work,
                    document: state.currEmployee.document,
                    wage: state.currEmployee.wage,
                }
            });
        } break;
        case EmployeeActions.CRUD_EMP_DOCUMENT_FAILED: {
            return extend({}, state, {});
        } break;
        case EmployeeActions.CRUD_EMP_WAGE: {
            return extend({}, state, {
                empWageData: {}, currEmployee: {
                    id: state.currEmployee.id, master: state.currEmployee.master, personal: state.currEmployee.personal,
                    bank: state.currEmployee.bank, work: state.currEmployee.work, document: state.currEmployee.document, wage: action.data
                }
            });
        } break;
        case EmployeeActions.CRUD_EMP_WAGE_SUCCESS: {
            return extend({}, state, {
                empWageData: { data: action.data }, currEmployee: {
                    id: state.currEmployee.id,
                    master: state.currEmployee.master,
                    personal: state.currEmployee.personal,
                    bank: state.currEmployee.bank,
                    work: state.currEmployee.work,
                    document: state.currEmployee.document,
                    wage: state.currEmployee.wage,
                    done: true
                }
            });
        } break;
        case EmployeeActions.CRUD_EMP_WAGE_FAILED: {
            return extend({}, state, {});
        } break;
        case EmployeeActions.FILE_UPLOAD: {
            return extend({}, state, {});
        } break;
        case EmployeeActions.FILE_UPLOAD_SUCCESS: {
            let temp = JSON.parse(JSON.stringify(state));
                let flag = false;
                for(var i = 0; i < 3; i++) {
                    if(temp.documents[i] && temp.documents[i].document_id == temp.currentDocument) {
                        flag = true;
                        break;
                    }
                }
                if(flag) {
                    temp.documents[i] = { document_id: temp.currentDocument, employee_id: temp.currEmployee.id, value: JSON.parse(action.data).url}
                } else {
                    temp.documents.push({ document_id: temp.currentDocument, employee_id: temp.currEmployee.id, value: JSON.parse(action.data).url });
                }
            
            return extend({}, state, temp);
        } break;
        case EmployeeActions.FILE_UPLOAD_FAILED: {
            return extend({}, state, {});
        } break;
        case EmployeeActions.CURRENT_DOCUMENT: {
            return extend({}, state, { currentDocument: action.data });
        } break;
        case EmployeeActions.DELETE_EMPLOYEE: {
            return extend({}, state, { });
        } break;
        case EmployeeActions.DELETE_EMPLOYEE_SUCCESS: {
            return extend({}, state, { });
        } break;
        case UserActions.LOGOUT: {
            return extend({}, state,
                {
                    listEmployees: [],
                    currentStep: 1,
                    currEmployee: { employee_name: '' },
                    empMasterData: {},
                    empPersonalData: {},
                    empBankData: {},
                    empWorkData: {},
                    empDocumentData: {},
                    empWageData: {},
                    fields: [],
                    states: [
                        { id: 1, name: 'Telangana' },
                        { id: 2, name: 'Maharashtra' },
                        { id: 3, name: 'Punjab' },
                        { id: 4, name: 'Karnataka' },
                    ],
                    designations: [
                        { id: 1, name: 'Accountant' }
                    ],
                    cities: [
                        { id: 1, state_id: 1, name: 'Hyderabad' },
                        { id: 2, state_id: 2, name: 'Pune' },
                        { id: 3, state_id: 2, name: 'Mumbai' },
                        { id: 4, state_id: 2, name: 'Nasik' },
                        { id: 5, state_id: 3, name: 'Amritsar' },
                        { id: 6, state_id: 3, name: 'Ludhiana' },
                        { id: 7, state_id: 3, name: 'Mohali' },
                        { id: 8, state_id: 4, name: 'Bangalore' },
                    ],
                    maritalStatuses: [
                        { id: 0, name: 'Unmarried' },
                        { id: 1, name: 'Married' },
                    ],
                    bloodGroups: [
                        { id: 0, name: 'O-' },
                        { id: 1, name: 'O+' },
                        { id: 2, name: 'A-' },
                        { id: 3, name: 'A+' },
                        { id: 4, name: 'B-' },
                        { id: 5, name: 'B+' },
                        { id: 6, name: 'AB-' },
                        { id: 7, name: 'AB+' },
                    ],
                    taxonomies: [
                        { id: 1, name: 'Allowance' },
                        { id: 2, name: 'Deduction' },
                        { id: 3, name: 'Company Contribution' },
                    ],
                    fields: [
                        { id: 1, taxonomy_id: 1, name: "HRA", type: 0, value: 0 },
                        { id: 2, taxonomy_id: 1, name: "HDA", type: 0, value: 0 },
                        { id: 3, taxonomy_id: 1, name: "Washing", type: 1, value: 0 },
                        { id: 4, taxonomy_id: 1, name: "Conveyance", type: 0, value: 0 },
                        { id: 5, taxonomy_id: 1, name: "CCA", type: 0, value: 0 },
                        { id: 6, taxonomy_id: 1, name: "Additional Salary", type: 0, value: 0 },
                        { id: 7, taxonomy_id: 2, name: "PF", type: 0, value: 0 },
                        { id: 8, taxonomy_id: 2, name: "ESIC", type: 0, value: 0 },
                        { id: 9, taxonomy_id: 2, name: "MLWF", type: 0, value: 0 },
                        { id: 10, taxonomy_id: 2, name: "Professional Tax", type: 1, value: 0 },
                        { id: 11, taxonomy_id: 3, name: "PF", type: 0, value: 0 },
                        { id: 12, taxonomy_id: 3, name: "ESIC", type: 0, value: 0 },
                        { id: 13, taxonomy_id: 3, name: "Ex-Gratia", type: 0, value: 0 },
                        { id: 14, taxonomy_id: 3, name: "Leave Salary", type: 1, value: 0 },
                        { id: 15, taxonomy_id: 3, name: "MLWF", type: 0, value: 0 },
                        { id: 16, taxonomy_id: 3, name: "Gratuity", type: 1, value: 0 },
                        { id: 17, taxonomy_id: 3, name: "Documentation Charges", type: 1, value: 0 },
                        { id: 18, taxonomy_id: 3, name: "Training Charges", type: 1, value: 0 },
                        { id: 19, taxonomy_id: 3, name: "Uniforms", type: 1, value: 0 },
                    ],
                    values: [
                        { term_id: 1, value: 0 },
                        { term_id: 2, value: 0 },
                        { term_id: 3, value: 0 },
                        { term_id: 4, value: 0 },
                        { term_id: 5, value: 0 },
                        { term_id: 6, value: 0 },
                        { term_id: 7, value: 0 },
                        { term_id: 8, value: 0 },
                        { term_id: 9, value: 0 },
                        { term_id: 10, value: 0 },
                        { term_id: 11, value: 0 },
                        { term_id: 12, value: 0 },
                        { term_id: 13, value: 0 },
                        { term_id: 14, value: 0 },
                        { term_id: 15, value: 0 },
                        { term_id: 16, value: 0 },
                        { term_id: 17, value: 0 },
                        { term_id: 18, value: 0 },
                        { term_id: 19, value: 0 },
                    ],
                    currentDocument: 0,
                    step5Documents: [
                    ]
                }
            );
        }
        default:
            return state;
    }
};
