import { User } from './user_reducer';
import { Employees } from './employee_reducer';
import { combineReducers } from 'redux';


let AppReducer = combineReducers({
    User, Employees
});
export default AppReducer;