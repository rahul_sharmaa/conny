/* eslint-env node */

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var del = require('del');
var express = require('express');
var ghPages = require('gh-pages');
var packageJson = require('./package.json');
var path = require('path');
var runSequence = require('run-sequence');
var swPrecache = require('./lib/sw-precache.js');
var minify = require('gulp-minify');
var insert = require('gulp-insert');

var DEV_DIR = 'src';
var DIST_DIR = 'build';

function runExpress(port, rootDir) {
    var app = express();

    app.use(express.static(rootDir));
    app.set('views', path.join(rootDir, 'views'));
    app.set('view engine', 'jade');

    app.get('/dynamic/:page', function(req, res) {
        res.render(req.params.page);
    });

    var server = app.listen(port, function() {
        var host = server.address().address;
        var port = server.address().port;
        console.log('Server running at http://%s:%s', host, port);
    });
}

function writeServiceWorkerFile(rootDir, handleFetch, callback) {
    var config = {
        cacheId: packageJson.name,
        /*
        dynamicUrlToDependencies: {
          'dynamic/page1': [
            path.join(rootDir, 'views', 'layout.jade'),
            path.join(rootDir, 'views', 'page1.jade')
          ],
          'dynamic/page2': [
            path.join(rootDir, 'views', 'layout.jade'),
            path.join(rootDir, 'views', 'page2.jade')
          ]
        },
        */
        // If handleFetch is false (i.e. because this is called from generate-service-worker-dev), then
        // the service worker will precache resources but won't actually serve them.
        // This allows you to test precaching behavior without worry about the cache preventing your
        // local changes from being picked up during the development cycle.
        handleFetch: handleFetch,
        logger: $.util.log,
        runtimeCaching: [{
            // See https://github.com/GoogleChrome/sw-toolbox#methods
            urlPattern: /runtime-caching/,
            handler: 'cacheFirst',
            // See https://github.com/GoogleChrome/sw-toolbox#options
            options: {
                cache: {
                    maxEntries: 1,
                    name: 'runtime-cache'
                }
            }
        }],
        staticFileGlobs: [
            rootDir + '/*.*',,
            rootDir + '/**/*.*',,

        ],
        stripPrefix: rootDir + '/',
        // verbose defaults to false, but for the purposes of this demo, log more.
        verbose: true
    };

    swPrecache.write(path.join(rootDir, 'service-worker.js'), config, callback);
}

gulp.task('default', ['serve-dist']);

gulp.task('build', function(callback) {
    runSequence('copy-dev-to-dist', 'generate-service-worker-dist', 'compressSW', callback);
});

gulp.task('clean', function() {
    del.sync([DIST_DIR]);
});

gulp.task('compressSW', function() {
    console.log('Service Worker Minification Started...');
    gulp.src('./build/service-worker.js')
        .pipe(minify({
            ext: {
                src: '',
                min: '.min.js'
            }
        }))
        .pipe(gulp.dest(DIST_DIR));
    console.log('Minified and placed in build folder as :->', '"service-worker.min.js"');
});

gulp.task('serve-dev', ['generate-service-worker-dev'], function() {
    runExpress(3001, DEV_DIR);
});

gulp.task('serve-dist', ['build'], function() {
    runExpress(3000, DIST_DIR);
});

gulp.task('gh-pages', ['build'], function(callback) {
    ghPages.publish(path.join(__dirname, DIST_DIR), callback);
});

gulp.task('generate-service-worker-dev', function(callback) {
    writeServiceWorkerFile(DEV_DIR, false, callback);
});

gulp.task('generate-service-worker-dist', function(callback) {
    writeServiceWorkerFile(DIST_DIR, true, callback);
});

gulp.task('copy-dev-to-dist', function() {
    return gulp.src(DEV_DIR + '/**')
        .pipe(gulp.dest(DIST_DIR));
});

//  Update HTMl after generating build

var inject = require('gulp-inject');
var htmlmin = require('gulp-htmlmin');
var inject_html_content = require('gulp-inject-string');
gulp.task('generate_html', function() {
    var jsFileName = '';
    var cssFileName = '';
    var jsTransform = function(filepath, file, i, length) {
        jsFileName = '/' + filepath.replace('build', '');
        return '<script src="/' + filepath.replace('build', '') + '" async defer></script>';
    };
    var cssTransform = function(filepath, file, i, length) {
        cssFileName = '/' + filepath.replace('build', ''); ;
        return '<link  rel="stylesheet" href="/' + filepath.replace('build', '') + '" ></link>';
    };
    var fontTransformWoff = function(filepath, file, i, length) {
        console.log(filepath, file, i, length);
        return '<link  rel="preload"   as="font" type="font/woff" href="' + filepath.replace('build', '') + '" ></link>';
    };
    var fontTransformTtf = function(filepath, file, i, length) {
        console.log(filepath, file, i, length);
        return '<link  rel="preload"   as="font" type="font/ttf" href="' + filepath.replace('build', '') + '" ></link>';
    };
    var imageTransformPng = function(filepath, file, i, length) {
        console.log(filepath, file, i, length);
        return '<link  rel="preload" as="image" href="' + filepath.replace('build', '') + '" ></link>';
    };
    var imageTransformIcon = function(filepath, file, i, length) {
        console.log(filepath, file, i, length);
        return '<link  rel="preload" as="image" href="' + filepath.replace('build', '') + '" ></link>';
    };
    var imageTransformGIF = function(filepath, file, i, length) {
        console.log(filepath, file, i, length);
        return '<link  rel="preload" as="image" href="' + filepath.replace('build', '') + '" ></link>';
    };
    gulp.src('./build/index.html')
        .pipe(inject(gulp.src(['./build/js/*.js'], { read: false }), { relative: true, transform: jsTransform }))
        .pipe(inject(gulp.src(['./build/css/*.css'], { read: false }), { relative: true, transform: cssTransform }))
        .pipe(inject_html_content.replace('<html>', '<html ⚡>'))
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('./build'));
});

const imagemin = require('gulp-imagemin');

gulp.task('minify_images', () =>
    gulp.src(['./build/media/*.svg', './build/media/*.png', './build/media/*.jpg', './build/media/*.gif'])
        .pipe(imagemin())
        .pipe(gulp.dest('/build/media/'))
);

