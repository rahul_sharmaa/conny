import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Header from './../components/header';
import { bindActionCreators } from 'redux';
import * as UserActions from './../actions/user_actions';
//import * as EmployeeActions from './../actions/employee_actions';

import NotificationSystem from 'react-notification-system';

import Footer from './../components/footer';

import './style.css';
var logo = require('./../images/connylogo.png');

let Landing = React.createClass({
    getInitialState() {
        return {
        };
    },
    componentWillMount() {
    },
    componentWillReceiveProps(nextProps) {
        console.log('back screen', nextProps);
        debugger
        this.setState({
            isCredsInvalid: true,
            credsError: 'Invalid email/password',
            successMsg: ''
        });
        if(nextProps && nextProps.userDetails && nextProps.userDetails.accessToken) {
            this.setState({
                isCredsInvalid: false,
                credsError: '',
                successMsg: 'Login Successful'
            });
        }
    },
    loginUser() {
        this.setState({
            isCredsInvalid: false,
            credsError: '',
        });
        var email = this.refs.fldEmail.value;
        var password = this.refs.fldPassword.value;
        if (email === "" || password === "") {
            this.setState({
                isCredsInvalid: true,
                credsError: 'Invalid Email Id/Password',
            });
        } else {
            var loginData = { strategy: 'user', email: email, password: password };
            this.props.dispatch(UserActions.userLogin((loginData)));
        }
    },
    render() {
        var _this = this;
        return (
            <div className="wrapper" >
                <div id="main-panel" className="main-panel">
                    <div className="top-content">
                        <div className="inner-bg">
                            <div className="container">

                                <div className="row">
                                    <div className="col-sm-6 col-sm-offset-3 form-box">
                                        <img src={logo} alt="" width="400" />
                                        <div className="form-top">
                                            <div className="form-top-left">
                                                <h3>Login to our site</h3>
                                                <p>Enter your username and password to log on:</p>
                                            </div>
                                            <div className="form-top-right">
                                                <i className="fa fa-lock"></i>
                                            </div>
                                        </div>
                                        <div className="form-bottom">
                                            {this.state.isCredsInvalid ? <div>{this.state.credsError}</div> : <div>{this.state.successMsg}</div>}
                                            <div className="form-group">
                                                <label className="sr-only" htmlFor="form-username">Username</label>
                                                <input type="text" ref="fldEmail" onChange={e => this.setState({ email: e.target.value })} value={this.state.email} name="form-username" placeholder="Email..." className="form-username form-control" id="form-username" />

                                            </div>
                                            <div className="form-group">
                                                <label className="sr-only" htmlFor="form-password">Password</label>
                                                <input type="password" ref="fldPassword" name="form-password" onChange={e => this.setState({ password: e.target.value })} value={this.state.password} placeholder="Password..." className="form-password form-control" id="form-password" />
                                            </div>
                                            <button type="submit" onClick={this.loginUser} className="btn">Sign in!</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
});
let mapStateToProps = (state) => {
    return ({ userDetails: state.User.userDetails, listEmployees: state.Employees });
};
let mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(Landing));
