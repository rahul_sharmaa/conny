
import React from 'react';

export var PageNotFound = React.createClass({
    render() {
        return (
            <div>
                <p>We're sorry...</p>
                <p>The page you are looking for cannot be found.</p>
            </div>

        );
    }
});

export default PageNotFound;
