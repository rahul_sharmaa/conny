import React, { Component } from 'react';

class LoginPage extends Component{
    constructor(props) {
        super(props);
        this.state = { email: '', password: '' };
    }

    handleSubmit =function(){
     
    }
    render() {
    return (
        <div className="top-content">
        <div className="inner-bg">
            <div className="container">

                <div className="row">
                    <div className="col-sm-6 col-sm-offset-3 form-box">
                        <img src="connylogo.png" alt="" width="400"/>
                        <div className="form-top">
                        <div className="form-top-left">
                            <h3>Login to our site</h3>
                            <p>Enter your username and password to log on:</p>
                        </div>
                        <div className="form-top-right">
                            <i className="fa fa-lock"></i>
                        </div>
                        </div>
                        <div className="form-bottom">
                            <form role="form" onSubmit={this.handleSubmit}  className="login-form">
                            <div className="form-group">
                                <label className="sr-only" htmlFor="form-username">Username</label>
                                <input type="text"  onChange={e => this.setState({ email: e.target.value })} value={this.state.email}name="form-username" placeholder="Email..." className="form-username form-control" id="form-username"/>

                            </div>
                            <div className="form-group">
                                <label className="sr-only" htmlFor="form-password">Password</label>
                                <input type="password" name="form-password"onChange={e => this.setState({ password: e.target.value })} value={this.state.password} placeholder="Password..." className="form-password form-control" id="form-password"/>
                            </div>
                            <button type="submit" className="btn">Sign in!</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        </div>
    );
  }
}

export default LoginPage;
