import Dashboard from '../views/Dashboard/Dashboard';
import UserProfile from '../views/UserProfile/UserProfile';
import TableList from '../views/TableList/TableList';
import Typography from '../views/Typography/Typography';
import Icons from '../views/Icons/Icons';
import Maps from '../views/Maps/Maps';
import Notifications from '../views/Notifications/Notifications';
import EquipmentList from '../views/EquipmentList/EquipmentList';
import EditPage from '../views/EditPage/EditPage';
import LoginPage from '../views/LoginPage/LoginPage';
import Upgrade from '../views/Upgrade/Upgrade';

const appRoutes = [
    { path: "/dashboard", name: "Dashboard", icon: "pe-7s-look", component: Dashboard },
    { path: "/list", name: "User List", icon: "pe-7s-users", component: TableList },
    { path: "/Equipment", name: "Equipment List", icon: "pe-7s-tools", component: EquipmentList },
    { path: "/edit", name: "EditPage (temp)", icon: "pe-7s-note", component: EditPage },
    { path: "/login", name: "LoginPage (temp)", icon: "pe-7s-door-lock", component: LoginPage },
    { path: "/user", name: "User Profile", icon: "pe-7s-user", component: UserProfile },
    { path: "/typography", name: "Typography", icon: "pe-7s-news-paper", component: Typography },
    { path: "/icons", name: "Icons", icon: "pe-7s-science", component: Icons },
    { path: "/maps", name: "Maps", icon: "pe-7s-map-marker", component: Maps },
    { path: "/notifications", name: "Notifications", icon: "pe-7s-bell", component: Notifications },
    //{ upgrade: true, path: "/upgrade", name: "Upgrade to PRO", icon: "pe-7s-rocket", component: Upgrade },
    { redirect: true, path:"/", to:"/dashboard", name: "Dashboard" }
];

export default appRoutes;
