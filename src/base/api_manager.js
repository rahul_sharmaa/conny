import {
    success_manager
} from './success_manager';
import {
    failed_manager
} from './failed_manager';
//import * as UserActions from './../actions/user_actions';

export var response_Manager = (isNetworkError, dispatch,
    endpoint, payload, response, error) => {
        debugger
        console.log("manager started", response);
    if (!response) {
        return failed_manager(dispatch, endpoint,
            payload, response, error);
    
    } else if (!isNetworkError && response && response.status !== 201 ) {
        return failed_manager(dispatch, endpoint, payload, response);
    } else if (!isNetworkError && response && response.status === 201) {
        return success_manager(dispatch, endpoint, payload, response);
    }
    return failed_manager(dispatch, endpoint,
        payload, response, response || error);
};
