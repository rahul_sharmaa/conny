import * as api_urls from './../config/api_urls';
import * as UserActions from './../actions/user_actions';
import * as EmployeeActions from './../actions/employee_actions';

export var failed_manager =
    (dispatch, endpoint, payload, response, error) => {
        if (response && response.message === 'User is offline.') {
            alert('Please check your internet connection and try again.');
        }
        switch (endpoint) {
            case api_urls.USER_LOGIN: {
                dispatch(UserActions.loginFailed(response))
            } break;
            case api_urls.ADD_EMP_MASTER: {
                return dispatch(EmployeeActions.crudEmployeeMasterFailed(response));
            } break;
            case api_urls.ADD_EMP_PERSONAL: {
                return dispatch(EmployeeActions.crudEmployeePersonalFailed(response));
            } break;
            case api_urls.ADD_EMP_BANK: {
                return dispatch(EmployeeActions.crudEmployeeBankFailed(response));
            } break;
            case api_urls.ADD_EMP_WORK: {
                return dispatch(EmployeeActions.crudEmployeeWorkFailed(response));
            } break;
            case api_urls.ADD_EMP_DOCUMENT: {
                return dispatch(EmployeeActions.crudEmployeeDocumentFailed(response));
            } break;
            case api_urls.ADD_EMP_WAGE: {
                return dispatch(EmployeeActions.crudEmployeeWageFailed(response));
            } break;
            case api_urls.UPLOAD_ASSET: {
                return dispatch(EmployeeActions.uploadAssetFailed(response));
            } break;
            default: {
                if (response) {
                    return response;
                } else if (error) {
                    return error;
                } else {
                    return {
                        status: 404,
                        message: 'Unable to reach server. Please try later.'
                    };
                }
            }
        }
    };
