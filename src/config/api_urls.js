// ------ Declare Constants Here --------//
export var BASE_URLs = 'http://localhost:82/helloepos/api/public/api'
export var BASE_URL = 'http://localhost:3030';
export var BASE_URLss = 'http://conny.ap.ngrok.io'
export var API_KEY='';
export var API_GET = 'GET';
export var API_POST = 'POST';

//-------  DECLARE ENDPOINTS HERE  --------//
export var USER_LOGIN = '/authentication' ;
export var FETCH_USERS = '/users/all' ;
export var GET_USERS = '/user';
//--------- RETURN FINAL URLS -------------//
export var get = (url, data) => {
    switch (url) {
        case USER_LOGIN:{
            return  BASE_URL + USER_LOGIN;
        }
        case FETCH_USERS:{
            return  BASE_URL + FETCH_USERS;
        }
        case GET_USERS:{
            return BASE_URL + GET_USERS;
        }
        default:
            break;
    }
    return 'URL Not configured';
};
