## Hello Pos Mobile Web App


## Prerequisites
    * Node - v6.6.0
    * NPM - v3.10.8

## Install Packages
    * Run command `npm install` on the project directory 
    to install equired packages.    


## Environments

    Development:
    1. Run command `npm start`.
    2. Open http://localhost:3000/ in browser.

    Production:
    1. Run command `npm run build_stage_win` for windows machines to generate a debuggable build with source maps.
    2. Run command `npm run build_stage` for linux machines to generate a debuggable build with source maps.
    3. Run command `npm run build_prod_win` for windows machines to generate a production build without source maps.
    4. Run command `npm run build_stage` for linux machines to generate a production build without source maps.
    5. Output will be copied to `./build` folder. 


## Service Worker

    We would need a service worker file and below command create the same.
    * `gulp generate-service-worker-dist` to create service worker.
    * `gulp compressSW` to minify the service-worker file.