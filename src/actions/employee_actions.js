import * as API from './../base/api_service';
import * as serviceCallConfig from './../config/api_urls';
import map from 'lodash/map';
import isArray from 'lodash/isArray';
import { ADD_EMP_MASTER } from './../config/api_urls';

import {
    success_manager
} from './../base/success_manager';
import {
    failed_manager
} from './../base/failed_manager';

export const LIST_ALL = 'LIST_ALL';
export const LIST_ALL_SUCCESS = 'LIST_ALL_SUCCESS';
export const GET_EMP = 'GET_EMP';
export const GET_EMP_SUCCESS = 'GET_EMP_SUCCESS';
export const NEW_EMPLOYEE = 'NEW_EMPLOYEE';
export const ADD_EMP_STEP = 'ADD_EMP_STEP';

export const FILE_UPLOAD = 'FILE_UPLOAD';
export const FILE_UPLOAD_SUCCESS = 'FILE_UPLOAD_SUCCESS';
export const FILE_UPLOAD_FAILED = 'FILE_UPLOAD_FAILED';

export const DELETE_EMPLOYEE = 'DELETE_EMPLOYEE';
export const DELETE_EMPLOYEE_SUCCESS = 'DELETE_EMPLOYEE_SUCCESS';

export const CRUD_EMP_MASTER = 'CRUD_EMP_MASTER';
export const CRUD_EMP_MASTER_SUCCESS = 'CRUD_EMP_MASTER_SUCCESS';
export const CRUD_EMP_MASTER_FAILED = 'CRUD_EMP_MASTER_FAILED';

export const CRUD_EMP_PERSONAL = 'CRUD_EMP_PERSONAL';
export const CRUD_EMP_PERSONAL_SUCCESS = 'CRUD_EMP_PERSNOAL_SUCCESS';
export const CRUD_EMP_PERSONAL_FAILED = 'CRUD_EMP_PERSONAL_FAILED';

export const CRUD_EMP_BANK = 'CRUD_EMP_BANK';
export const CRUD_EMP_BANK_SUCCESS = 'CRUD_EMP_BANK_SUCCESS';
export const CRUD_EMP_BANK_FAILED = 'CRUD_EMP_BANK_FAILED';

export const CRUD_EMP_WORK = 'CRUD_EMP_WORK';
export const CRUD_EMP_WORK_SUCCESS = 'CRUD_EMP_WORK_SUCCESS';
export const CRUD_EMP_WORK_FAILED = 'CRUD_EMP_WORK_FAILED';

export const CRUD_EMP_DOCUMENT = 'CRUD_EMP_DOCUMENT';
export const CRUD_EMP_DOCUMENT_SUCCESS = 'CRUD_EMP_DOCUMENT_SUCCESS';
export const CRUD_EMP_DOCUMENT_FAILED = 'CRUD_EMP_DOCUMENT_FAILED';

export const CRUD_EMP_WAGE = 'CRUD_EMP_WAGE';
export const CRUD_EMP_WAGE_SUCCESS = 'CRUD_EMP_WAGE_SUCCESS';
export const CRUD_EMP_WAGE_FAILED = 'CRUD_EMP_WAGE_FAILED';

export const LOAD_FIELDS = 'LOAD_FIELDS';
export const CURRENT_DOCUMENT = 'CURRENT_DOCUMENT';

export var listAll = () => {
    return (dispatch, getState) => {
        dispatch({
            type: LIST_ALL
        })
        API.makeServiceCall('GET', dispatch, serviceCallConfig.get(serviceCallConfig.EMP_LIST_ALL),
            null, serviceCallConfig.EMP_LIST_ALL, false, getState().User.userDetails.data.token);
    }
}

export var listAllSuccess = (list) => {
    return (dispatch, getState) => {
        dispatch({
            type: LIST_ALL_SUCCESS,
            data: list.data,
        })
    }
}

export var loadFields = (payload) => {
    return (dispatch, getState) => {
        dispatch({
            type: LOAD_FIELDS,
            data: payload
        });
    }
}

export var employeeLoadNext = (stepNo) => {
    return (dispatch, getState) => {
        dispatch({
            type: ADD_EMP_STEP,
            data: stepNo
        });
    }
}

export var refreshAddEmployee = () => {
    return (dispatch, getState) => {
        dispatch({
            type: NEW_EMPLOYEE
        })
    }
}

export var deleteEmployee = (payload) => {
    return (dispatch, getState) => {
        var token = getState().User.userDetails.data.token
        dispatch({
            type: DELETE_EMPLOYEE,
        })
        API.makeServiceCall('POST', dispatch, serviceCallConfig.get(serviceCallConfig.DELETE_EMPLOYEE),
        payload, serviceCallConfig.DELETE_EMPLOYEE, false, token);
    }
}

export var deleteEmployeeSuccess = (data) => {
    return (dispatch, getState) => {
        dispatch({
            type: DELETE_EMPLOYEE_SUCCESS,
            data: data,
        })
    }
}

export var getEmployeeDetail = (id) => {
    return (dispatch, getState) => {
        var token = getState().User.userDetails.data.token
        dispatch({
            type: GET_EMP,
        })
        API.makeServiceCall('GET', dispatch, serviceCallConfig.get(serviceCallConfig.GET_EMP) + "/" + id,
            null, serviceCallConfig.GET_EMP, false, token);
    }
}

export var getEmployeeDetailSuccess = (data) => {
    return (dispatch, getState) => {
        dispatch({
            type: GET_EMP_SUCCESS,
            data: data,
        })
    }
}

export var crudEmployeeMaster = (payloadData) => {
    return (dispatch, getState) => {
        var token = getState().User.userDetails.data.token
        dispatch({
            type: CRUD_EMP_MASTER,
            data: payloadData
        })
        API.makeServiceCall('POST', dispatch, serviceCallConfig.get(serviceCallConfig.ADD_EMP_MASTER),
            payloadData, serviceCallConfig.ADD_EMP_MASTER, false, token);
    }
}

export var crudEmployeeMasterSuccess = (empData) => {
    return (dispatch, getState) => {
        dispatch({
            type: CRUD_EMP_MASTER_SUCCESS,
            data: empData
        })
    }
}

export var crudEmployeeMasterFailed = () => {
    return (dispatch, getState) => {
        dispatch({
            type: CRUD_EMP_MASTER_FAILED
        })
    }
}

export var crudEmployeePersonal = (payloadData) => {
    return (dispatch, getState) => {
        var token = getState().User.userDetails.data.token
        dispatch({
            type: CRUD_EMP_PERSONAL,
            data: payloadData
        })
        API.makeServiceCall('POST', dispatch, serviceCallConfig.get(serviceCallConfig.ADD_EMP_PERSONAL),
            payloadData, serviceCallConfig.ADD_EMP_PERSONAL, false, token);
    }
}

export var crudEmployeePersonalSuccess = (empData) => {
    return (dispatch, getState) => {
        dispatch({
            type: CRUD_EMP_PERSONAL_SUCCESS,
            data: empData
        })
    }
}

export var crudEmployeePersonalFailed = () => {
    return (dispatch, getState) => {
        dispatch({
            type: CRUD_EMP_PERSONAL_FAILED
        })
    }
}

export var crudEmployeeBank = (payloadData) => {
    return (dispatch, getState) => {
        var token = getState().User.userDetails.data.token
        dispatch({
            type: CRUD_EMP_BANK,
            data: payloadData
        })
        API.makeServiceCall('POST', dispatch, serviceCallConfig.get(serviceCallConfig.ADD_EMP_BANK),
            payloadData, serviceCallConfig.ADD_EMP_BANK, false, token);
    }
}

export var crudEmployeeBankSuccess = (empData) => {
    return (dispatch, getState) => {
        dispatch({
            type: CRUD_EMP_BANK_SUCCESS,
            data: empData
        })
    }
}

export var crudEmployeeBankFailed = () => {
    return (dispatch, getState) => {
        dispatch({
            type: CRUD_EMP_BANK_FAILED
        })
    }
}

export var crudEmployeeWork = (payloadData) => {
    return (dispatch, getState) => {
        var token = getState().User.userDetails.data.token
        dispatch({
            type: CRUD_EMP_WORK,
            data: payloadData
        })
        API.makeServiceCall('POST', dispatch, serviceCallConfig.get(serviceCallConfig.ADD_EMP_WORK),
            payloadData, serviceCallConfig.ADD_EMP_WORK, false, token);
    }
}

export var crudEmployeeWorkSuccess = (empData) => {
    return (dispatch, getState) => {
        dispatch({
            type: CRUD_EMP_WORK_SUCCESS,
            data: empData
        })
    }
}

export var crudEmployeeWorkFailed = () => {
    return (dispatch, getState) => {
        dispatch({
            type: CRUD_EMP_WORK_FAILED
        })
    }
}

export var crudEmployeeDocument = (payloadData) => {
    return (dispatch, getState) => {
        var token = getState().User.userDetails.data.token
        dispatch({
            type: CRUD_EMP_DOCUMENT,
            data: payloadData
        })
        API.makeServiceCall('POST', dispatch, serviceCallConfig.get(serviceCallConfig.ADD_EMP_DOCUMENT),
            payloadData, serviceCallConfig.ADD_EMP_DOCUMENT, false, token);
    }
}

export var crudEmployeeDocumentSuccess = (empData) => {
    return (dispatch, getState) => {
        dispatch({
            type: CRUD_EMP_DOCUMENT_SUCCESS,
            data: empData
        })
    }
}

export var crudEmployeeDocumentFailed = () => {
    return (dispatch, getState) => {
        dispatch({
            type: CRUD_EMP_DOCUMENT_FAILED
        })
    }
}

export var crudEmployeeWage = (payloadData) => {
    return (dispatch, getState) => {
        var token = getState().User.userDetails.data.token
        dispatch({
            type: CRUD_EMP_WAGE,
            data: payloadData
        })
        API.makeServiceCall('POST', dispatch, serviceCallConfig.get(serviceCallConfig.ADD_EMP_WAGE),
            payloadData, serviceCallConfig.ADD_EMP_WAGE, false, token);
    }
}

export var crudEmployeeWageSuccess = (empData) => {
    return (dispatch, getState) => {
        dispatch({
            type: CRUD_EMP_WAGE_SUCCESS,
            data: empData
        })
    }
}

export var crudEmployeeWageFailed = () => {
    return (dispatch, getState) => {
        dispatch({
            type: CRUD_EMP_WAGE_FAILED
        })
    }
}

export var uploadAsset = (payload, docId) => {
    var formData = new FormData();
    formData.append('asset', payload);
    formData.append('doc_id', docId);
    return (dispatch, getState) => {
        var token = getState().User.userDetails.data.token
        dispatch({
            type: FILE_UPLOAD
        })
        sendXHRRequest(serviceCallConfig.get(serviceCallConfig.UPLOAD_ASSET), serviceCallConfig.UPLOAD_ASSET, formData, dispatch);
    }
}

var sendXHRRequest = (url, endPoint, payload, dispatch) => {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url , true);

    xhr.upload.onprogress = function (e) {
        if (e.lengthComputable) {
            var percentComplete = (e.loaded / e.total) * 100;
        }
    };
    xhr.onload = function () {
        if (this.status == 200) {
            success_manager(dispatch, endPoint, payload, this.response)
        };
    };
    xhr.send(payload);
}

export var uploadAssetSuccess = (data) => {
    return (dispatch, getState) => {
        dispatch({
            type: FILE_UPLOAD_SUCCESS,
            data: data
        })
    }
}

export var uploadAssetFailed = () => {
    return (dispatch, getState) => {
        dispatch({
            type: FILE_UPLOAD_FAILED
        })
    }
}

export var changeCurrentDocument = (data) => {
    return (dispatch, getState) => {
        dispatch({
            type: CURRENT_DOCUMENT,
            data: data
        })
    }
}